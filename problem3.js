function getItemsContains(dataset,vitamin) {
    return dataset.filter((item)=>{
        return item.contains.includes(vitamin);
    })
}


module.exports = getItemsContains;