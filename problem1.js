// 1. Get all items that are available 

function availableItems(dataset) {
    return dataset.filter((item)=>{
        return item.available
    })
}



module.exports = availableItems;