function getItemWithOnlyVitaminK(dataset){
    return dataset.filter((item)=>{
        return item.contains == 'Vitamin C';
    })
}


module.exports = getItemWithOnlyVitaminK;