function makeItGroup(dataset){
    return dataset.reduce((acu,cv)=>{
        return cv.contains.split(',').reduce((accu1,vitamin)=>{
            if(!accu1[vitamin.trim()]){
                accu1[vitamin.trim()] = [cv.name];
            }
            accu1[vitamin.trim()].push(cv.name);
            return accu1;
        },acu);
    },{});
}

module.exports = makeItGroup